# Magento Docker Compose

### Step 1
```bash
git clone https://github.com/magento/magento2.git    
```

### Step 2
```bash
cd magento2

git checkout 2.3

touch auth.json
```

```json
{
    "http-basic": {
        "repo.magento.com": {
            "username": "<public-key>",
            "password": "<secret-key>"
        }
    }
}

```

### Step 3
```bash
docker compose up -d
```

```bash
docker compose exec php bash

find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} + \
    && find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} + \
    && chown -R :www-data $(ls -Iwww-data)

# if first time
composer install

exit
```

### Step 4
```bash
docker compose exec -u magento php bash

php bin/magento setup:install \
    --base-url=http://localhost:8011 \
    --db-host=mysql \
    --db-name=magento_db \
    --db-user=magento \
    --db-password=secret \
    --backend-frontname=admin_mn \
    --admin-firstname=admin \
    --admin-lastname=admin \
    --admin-email=admin@example.com \
    --admin-user=admin \
    --admin-password=admin123 \
    --language=en_US \
    --currency=USD \
    --timezone=America/Chicago \
    --use-rewrites=1
```

### Step 5 
Intall native nginx

```bash
sudo yum install nginx

sudo systemctl enable nginx

sudo systemctl start nginx
```

### Step 6
Create new NGINX config

```bash
vi /etc/nginx/conf.d/magento2.conf
```

```conf
server {
    listen 80;
    listen [::]:80;

    server_name magento.pxuanbach.id.vn;

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:8011/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        include /etc/nginx/proxy_params;
    }
}
```

Update `/etc/hosts`
```bash
vi /etc/hosts
```

```conf
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.0.1   magento.pxuanbach.id.vn
```

### Step 7
Update Magento Env
```bash
docker compose exec -u magento php bash

php bin/magento config:set web/unsecure/base_url https://magento.pxuanbach.id.vn/
php bin/magento config:set web/secure/base_url https://magento.pxuanbach.id.vn/
php bin/magento config:set web/secure/use_in_frontend 1
php bin/magento config:set web/secure/use_in_adminhtml 1

php bin/magento cache:flush

exit
```

### Step 8
Point domain name to IP, divide subdomain names

### Step 9
Enable SSL 
https://www.linuxtrainingacademy.com/install-ssl-certificate-almalinux-nginx-lets-encrypt/
