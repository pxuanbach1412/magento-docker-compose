php bash

cd /var/www/html/magento

find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} + \
  && find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} + \
  && chown -R :www-data $(ls -Iwww-data)

# Cài đặt magento db
php bin/magento setup:install \
  --base-url=http://localhost:8011 \
  --db-host=mysql \
  --db-name=magento_db \
  --db-user=magento \
  --db-password=secret \
  --backend-frontname=admin_mn \
  --admin-firstname=admin \
  --admin-lastname=admin \
  --admin-email=admin@example.com \
  --admin-user=admin \
  --admin-password=admin123 \
  --language=en_US \
  --currency=USD \
  --timezone=America/Chicago \
  --use-rewrites=1